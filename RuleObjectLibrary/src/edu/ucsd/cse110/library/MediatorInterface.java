package edu.ucsd.cse110.library;

public interface MediatorInterface {
	public void checkoutPublication(Member mem, Publication pub);
	public void returnPublication(Publication pub);
	public double getMemberFees(Member mem);
	public boolean checkMemberFees(Member mem);
}