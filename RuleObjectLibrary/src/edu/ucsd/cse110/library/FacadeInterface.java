package edu.ucsd.cse110.library;

public class FacadeInterface {

	public static void checkoutPublication(Member mem, Publication pub);
	public static void returnPublication(Publication pub);
	public static double getFee(Member mem);
	public static boolean hasFee(Member mem);


}