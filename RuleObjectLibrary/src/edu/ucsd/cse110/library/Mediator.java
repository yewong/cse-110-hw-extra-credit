package edu.ucsd.cse110.library;
import java.time.LocalDate;

public class Mediator implements MediatorInterface{

	public void checkoutPublication(Member mem, Publication pub) {
		LocalDate time = LocalDate.now();
		if(pub.isCheckOut() == False)
			pub.checkout(mem,time)
	}

	public void returnPublication(Publication pub){
		LocalDate time = LocalDate.now();
		pub.pubReturn(time);
	}

	public double getMemberFees(Member mem){
		double fee = mem.getDueFees();
		return fee;
	}

	public boolean checkMemberFees(Member mem){
		return m.getDueFees() !=0;
	}

}