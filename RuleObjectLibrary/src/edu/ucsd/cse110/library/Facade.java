import java.time.LocalDate;
package edu.ucsd.cse110.library;

public class Facade implements FacadeInterface {

	private Mediator m;



	public static void checkoutPublication(Member mem, Publication pub){
		m.checkoutPublication(mem, pub);
	}

	public static void returnPublication(Publication pub) {
		m.returnPublication(pub);
	}

	public static double getFee(Member mem) {
		return m.getMemberFees(mem);
	}

	public static boolean hasFee(Member mem){
		return m.checkMemberFees(mem);
	}


}